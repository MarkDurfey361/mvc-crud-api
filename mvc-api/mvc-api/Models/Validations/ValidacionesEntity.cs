﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mvc_api.Models
{
    [MetadataType(typeof(EmployeeTable.Metadata))]
    public partial class EmployeeTable
    {

        sealed class Metadata
        {

            public int EmpId { get; set; }
            
            [Required(ErrorMessage = "Nombre es requerido")]
            [MaxLength(100)]
            [MinLength(5)]
            public string EmpName { get; set; }
            
            [Required(ErrorMessage = "Email es requerido")]
            [EmailAddress(ErrorMessage = "Debe ser un Email válido")]
            [MaxLength(50)]
            [MinLength(5)]
            public string Email { get; set; }

            public Nullable<int> Age { get; set; }
                        
            public Nullable<int> Salary { get; set; }

        }

    }
}