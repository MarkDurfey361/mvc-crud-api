﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using mvc_api.Models;

namespace mvc_api.Controllers
{
    public class EmployeeTablesController : ApiController
    {
        private EMPEntities1 db = new EMPEntities1();

        // GET: api/EmployeeTables
        public IQueryable<EmployeeTable> GetEmployeeTable()
        {
            return db.EmployeeTable;
        }

        // GET: api/EmployeeTables/5
        [ResponseType(typeof(EmployeeTable))]
        public IHttpActionResult GetEmployeeTable(int id)
        {
            EmployeeTable employeeTable = db.EmployeeTable.Find(id);
            if (employeeTable == null)
            {
                return NotFound();
            }

            return Ok(employeeTable);
        }

        // PUT: api/EmployeeTables/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEmployeeTable(int id, EmployeeTable employeeTable)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != employeeTable.EmpId)
            {
                return BadRequest();
            }

            db.Entry(employeeTable).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeTableExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EmployeeTables
        [ResponseType(typeof(EmployeeTable))]
        public IHttpActionResult PostEmployeeTable(EmployeeTable employeeTable)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EmployeeTable.Add(employeeTable);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = employeeTable.EmpId }, employeeTable);
        }

        // DELETE: api/EmployeeTables/5
        [ResponseType(typeof(EmployeeTable))]
        public IHttpActionResult DeleteEmployeeTable(int id)
        {
            EmployeeTable employeeTable = db.EmployeeTable.Find(id);
            if (employeeTable == null)
            {
                return NotFound();
            }

            db.EmployeeTable.Remove(employeeTable);
            db.SaveChanges();

            return Ok(employeeTable);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeTableExists(int id)
        {
            return db.EmployeeTable.Count(e => e.EmpId == id) > 0;
        }
    }
}